import heapq


def read_input(file_path):
    with open(file_path, 'r') as f:
        lines = f.readlines()
        n = int(lines[0])
        graph = [[0 for j in range(n)] for i in range(n)]
        for i in range(1, n + 1):
            line = lines[i].split()
            for j in range(n):
                graph[i - 1][j] = int(line[j])
        return graph


def prim_mst(graph):
    n = len(graph)
    visited = [False] * n
    mst = []
    heap = [(0, 0)]  # (weight, node)

    while heap:
        weight, node = heapq.heappop(heap)
        if visited[node]:
            continue
        visited[node] = True
        mst.append((weight, node))
        for nei in range(n):
            if graph[node][nei] and not visited[nei]:
                heapq.heappush(heap, (graph[node][nei], nei))

    return mst


def result():
    file_path = 'data'
    graph = read_input(file_path)
    mst = prim_mst(graph)
    total_weight = 0
    print("Minimum Spanning Tree:\n")
    step = 0
    for weight, node in mst:
        step += 1
        print(f"Step: {step}\nNode: {node} - {weight}")
        total_weight += weight
        print("Minimum Spanning Tree weight:", total_weight, "\n")


if __name__ == '__main__':
    result()
